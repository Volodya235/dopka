# Написать функцию, которая:
# Принимает аргумент в виде числа (количество персонажей)
# Запрашивает в цикле персонажей с номером от 100 до 1000 (рандом)
# Сохраняет в файле имя персонажа ("name") и кто его играл ("playedBy").
# Если актера нет, то сохранить прочерк ("-")
import requests
from random import randint
from ast import literal_eval


def get_random_character(n):
    with open('output.txt', 'w') as outfile:
        for i in range(n):
            character_num = randint(100, 1000)
            data = literal_eval(requests.get(f'https://www.anapioficeandfire.com'
                                             f'/api/characters/{character_num}'
                                             ).content.decode())
            outfile.write(data['name'] + ' ')
            if data['playedBy'] == ['']:
                outfile.write('-')
            else:
                for name in data['playedBy']:
                    outfile.write(name + ' ')
            outfile.write('\n')


if __name__ == '__main__':
    get_random_character(5)
