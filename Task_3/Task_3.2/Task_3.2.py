"""
Найти топ 10 операторов с самой большим числом абонентов (колонка “емкость”).
К примеру, на этом скрине ниже у “Т2 Мобайл” уже 500 000 абонентов (суммируется
в каждой области). Пройдя по всей базе всего абонентов конечно же будет больше.

Ответ вывести списком кортежей [(оператор, количество абонентов), ...]
"""

result = {}
file_names = ['9_ABC', '3_ABC', '4_ABC', '8_ABC']
for file in file_names:
    with open(file + '.csv', 'r', encoding='utf-8') as csvfile:
        csvfile.readline()  # reading first info line
        for row_info in csvfile:
            info = row_info.split(';')
            if info[4] in result:
                result[info[4]] += int(info[3])
            else:
                result[info[4]] = int(info[3])

print(sorted(list(result.items()), key=lambda x: x[1], reverse=True)[:10])
