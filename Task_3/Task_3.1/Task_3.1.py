"""
Вам дан (файл “mobiles”) рандомный список телефонов. Нужно проверить по базе
(те 4 файла) к какому региону относится номер и вернуть ответ в виде списка
кортежей (телефон, регион).

К примеру, номер 8 900 0154351 относится к Тверской обл. и ответ будет:
[(89000154351, “Тверская обл.”), … ]
Объяснение почему так: 8 900 0154351 входит в диапазон оператора/области с
8 900 0100000 по 8 900 0199999

Если такого номера в базе нет, в области написать “номер неопознан”.
"""
import re


numbers = []
with open('mobiles', 'r') as mobiles:
    for number in mobiles:
        # deleting redundant chars and first 8
        numbers.append(re.sub(r'\D+', '', number)[1:])

result = []
file_names = ['9_ABC', '3_ABC', '4_ABC', '8_ABC']
for number in numbers:
    appended = False
    for file in file_names:
        with open(file + '.csv', 'r', encoding='utf-8') as csvfile:
            csvfile.readline()  # reading first info line
            for row_info in csvfile:
                if number[0:3] != row_info[0:3]:
                    continue
                elif (int(row_info.split(';')[1]) <=
                      int(number[3:]) <= int(row_info.split(';')[2])):
                    result.append(('8' + number, row_info.split(';')[5]))
                    appended = True
                    break
        if appended:
            break

    if not appended:
        result.append(('8' + number, 'номер неопознан'))
    print('Number done')

print(*result)