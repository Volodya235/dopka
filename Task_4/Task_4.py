from math import sqrt

from matplotlib import pyplot as plt


house_count = {}


def distance(x1, y1, x2, y2):
	"""
	Расчет расстояния между двумя точками
	Args:
		x1 y1 : Координаты 1-ой точки
		x2 y2 : Координаты 2-ой точки
	Returns:
		float: расстояние между ними
	"""
	return sqrt(abs(x1-x2)**2 + abs(y1-y2)**2)


def read_data(path):
	"""
	Чтение файла и сохранение в словаре вида: ключ адрес, значения это кортеж (координата Х, коордианата Y, площадь)
	Пример: {'пр-кт Фатыха Амирхана д 91Б': (5.73063, 11.8712, 2095.0), ... }
	Args:
		path (str): путь к файлу
	Returns:
		dict: ключ адрес, значение (x, y, площадь)
	"""
	database = {}
	with open(path, 'r', encoding='utf-8') as file:
		for line in file:
			args = line.split('\t')
			database[args[0]] = (float(args[1]), float(args[2]), float(args[3]))
	return database


def task1(database):
	"""
	Задача 1
	Args:
		database (dict): изначальный датасет словарь, ключ адрес, значение (x, y, площадь)
		помимо database могут быть любые другие аргументы
	Returns:
		list: координаты дома (x, y)
	"""
	result = [()]
	max_count = 0
	count = 0
	for building, coords in database.items():
		for address, addr_args in database.items():
			if distance(coords[0], coords[1], addr_args[0], addr_args[1]) <= 0.5:
				count += 1

		house_count[(coords[0], coords[1])] = count

		if count > max_count:
			max_count = count
			result[0] = (coords[0], coords[1])
		count = 0

	return result


def task2(database: dict):
	"""
	Задача 2
	Args:
		database (dict): изначальный датасет словарь, ключ адрес, значение (x, y, площадь)
		помимо database могут быть любые другие аргументы
	Returns:
		list: координаты домов [(x1,y1), (x2,y2) ... (xn,yn)]
	"""
	result = []
	global house_count
	coords_desc = sorted(house_count, key=house_count.__getitem__, reverse=True)
	result.append(coords_desc[0])
	for coords in coords_desc[1:]:
		all_good = True
		for house in result:
			if distance(coords[0], coords[1], house[0], house[1]) <= 1:
				all_good = False
				break

		if all_good:
			result.append(coords)
		if len(result) == 10:
			return result

	return result


def plot(database, best_coords):
	"""
	НЕ МЕНЯТЬ КОД!
	Отрисовка точек 2D
	Args:
		database (dict): изначальный датасет словарь, ключ адрес, значение (x, y, площадь)
		best_coords (list): для задачи 1 это (x, y), для задачи 2-3 это [(x1,y1), (x2,y2) ... (xn,yn)]
	"""
	plt.close()
	fig, ax = plt.subplots(figsize=(8, 8))
	plt.plot([coord[0] for coord in database.values()],
	         [coord[1] for coord in database.values()], '.', ms=5, color='k', alpha=0.5)
	if isinstance(best_coords[0], tuple):
		for x, y in best_coords:
			circle = plt.Circle((x, y), 0.5, color='r', fill=False, zorder=2)
			ax.add_patch(circle)
		plt.plot([coord[0] for coord in best_coords],
		         [coord[1] for coord in best_coords], '.', ms=15, color='r')
	elif isinstance(best_coords[0], float):
		x, y = best_coords
		circle = plt.Circle((x, y), 0.5, color='r', fill=False, zorder=2)
		ax.add_patch(circle)
		plt.plot(*best_coords, '.', ms=15, color='r')
	else:
		raise ValueError("Проверь, что подаёшь список кортежей или кортеж из двух координат")
	plt.show()


def homework():
	path = "buildings"
	database = read_data(path)

	best_task1 = task1(database)
	plot(database, best_task1)

	best_task2 = task2(database)
	plot(database, best_task2)


if __name__ == '__main__':
	homework()


