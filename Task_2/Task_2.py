import pickle
from collections import defaultdict


# top 10 слов длиной > 5 букв из всех сообщений пользователей (ключ body)
def func1(data: list) -> list:
    words = defaultdict(int)
    for msg in data:
        for word in msg['body']:
            words[word] += 1

    words = dict(reversed(sorted(words.items(), key=lambda item: item[1])))
    result = []
    for k, v in words[:11]:
        result.append(v)

    return result


# Возвращает число, а именно среднюю длину сообщения пользователей. Использовать
# в этой функции map, len, sum
def func2(data):
    sum_of_msgs = sum(list(map(lambda msg: len(msg['body']), data)))
    return sum_of_msgs / 441768


# Возвращает два списка:
# список топ 10 комментариев (текста) по рейтингу, а-ля “лайков” (ключ “score”),
# которые не были отредактированы
# (ключ “edited“), список авторов этих комментариев (ключ “author”)
def func3(data):
    msg_score_and_author = list(map(lambda msg: (msg['score'], msg['author'],
                                                 msg['body'])
                                    if msg['edited'] == 0 else (0, ''), data))
    sorted_msgs = reversed(sorted(msg_score_and_author, key=lambda x: x[0]))
    messages = []
    authors = []
    for message in sorted_msgs[:11]:
        messages.append(message[2])
        authors.append(message[1])

    return messages, authors


#  Возвращает список сообщений (текста) топ 10-ти комментариев с самым низким
#  рейтингом (ключ “score”)
def func4(data):
    msg_scores = list(map(lambda msg: (msg['score'], msg['body']), data))
    lowest_upvote_score = sorted(msg_scores, key=lambda x: x[0])
    result = []
    for msg in lowest_upvote_score[:11]:
        result.append(msg[1])
    return result


def func5(data, word):
    word_in_msg = list(map(lambda msg: word.lower() in msg['body'].lower(),
                           data))
    return word_in_msg.count(True) / 441768


# Вернуть количество сообщений в которых встречаются цифры
def func6(data):
    count = 0
    for msg in data:
        count += 1 if any(map(str.isdigit, msg['body'])) else 0

    return count


with open('data.pickle', 'rb') as file:
    data = pickle.load(file)
